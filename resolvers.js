import { ObjectId } from 'promised-mongo';

const { query } = require('nact');

const resource = 'vk_group' || 'tg_chat';
module.exports = {

  Mutation: {
    changeVkGroup: async (obj, args, ctx, info) => {
      const collectionItemActor = ctx.children.get('item');

      if (args._id) {
        return await query(collectionItemActor, { type: 'vk_group', _id: args._id, input: args.input }, global.actor_timeout);
      }
      return await query(collectionItemActor, { type: 'vk_group', input: args.input }, global.actor_timeout);
    },

    changeTgChat: async (obj, args, ctx, info) => {
      const collectionItemActor = ctx.children.get('item');

      if (args._id) {
        return await query(collectionItemActor, { type: 'tg_chat', search: { _id: args._id }, input: args.input }, global.actor_timeout);
      }
      return await query(collectionItemActor, { type: 'tg_chat', input: args.input }, global.actor_timeout);
    },
    changeRepostTransaction: async (obj, args, ctx, info) => {
      const collectionItemActor = ctx.children.get('item');

      if (args._id) {
        return await query(collectionItemActor, { type: 'repost_transaction', search: { _id: args._id }, input: args.input }, global.actor_timeout);
      }
      return await query(collectionItemActor, { type: 'repost_transaction', input: args.input }, global.actor_timeout);
    },
    changeTelegramRepost: async (obj, args, ctx, info) => {
      const collectionItemActor = ctx.children.get('item');
      if (args.input.from_id) {
        args.input.from_id = new ObjectId(args.input.from_id);
      }
      if (args.input.to_id) {
        args.input.to_id = new ObjectId(args.input.to_id);
      }
      if (args._id) {
        return await query(collectionItemActor, { type: 'telegram_repost', search: { _id: args._id }, input: args.input }, global.actor_timeout);
      }
      return await query(collectionItemActor, { type: 'telegram_repost', input: args.input }, global.actor_timeout);
    },
    createRepostTransaction: async (obj, args, ctx, info) => await query(collectionItemActor, { type: 'repost_transaction', input: args.input }, global.actor_timeout),
  },
  Query: {

    getVkGroup: async (obj, args, ctx, info) => {
      const collectionItemActor = ctx.children.get('item');

      return await query(collectionItemActor, { type: 'vk_group', search: { _id: args._id } }, global.actor_timeout);
    },
    getVkGroups: async (obj, args, ctx, info) => {
      const collectionActor = ctx.children.get('collection');

      return await query(collectionActor, { type: 'vk_group' }, global.actor_timeout);
    },

    getTgChat: async (obj, args, ctx, info) => {
      const collectionItemActor = ctx.children.get('item');

      return await query(collectionItemActor, { type: 'tg_chat', search: { _id: args._id } }, global.actor_timeout);
    },
    getTgChatByTelegramId: async (obj, args, ctx, info) => {
      const collectionItemActor = ctx.children.get('item');

      return await query(collectionItemActor, { type: 'tg_chat', search: { chat_id: args.telegram_id } }, global.actor_timeout);
    },
    getTgChats: async (obj, args, ctx, info) => {
      const collectionActor = ctx.children.get('collection');

      return await query(collectionActor, { type: 'tg_chat' }, global.actor_timeout);
    },
    getTelegramRepost: async (obj, args, ctx, info) => {
      const collectionItemActor = ctx.children.get('item');

      return await query(collectionItemActor, { type: 'telegram_repost', search: { _id: args._id } }, global.actor_timeout);
    },
    getTelegramReposts: async (obj, args, ctx, info) => {
      const collectionActor = ctx.children.get('collection');

      return await query(collectionActor, { type: 'telegram_repost' }, global.actor_timeout);
    },
  },
  TelegramRepost: {
    from: async (obj, args, ctx, info) => (await ctx.db.tg_chat.find({ _id: new ObjectId(obj.from_id) }))[0],
    to: async (obj, args, ctx, info) => (await ctx.db.tg_chat.find({ _id: new ObjectId(obj.to_id) }))[0],
  },
};
